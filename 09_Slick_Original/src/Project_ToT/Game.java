package Project_ToT;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import Project_ToT.Blocks;
public class Game extends BasicGame {

	private ArrayList<Actor> actors;
	private Player player1;
	public static final int GRID_SIZE = 50;
	
	
	public static int[][] multi = new int[][] {
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0},
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0}
		};
	
 	public Game() {
		super("Landscape");

	}

	@Override
	public void init(GameContainer gc) throws SlickException {

		this.actors = new ArrayList<>();
		this.player1 = new Player(100, 600, 0.2);
		this.actors.add(player1);
		
		
		for (int x = 0; x < multi.length; x++) {
			for (int y = 0; y < multi[0].length; y++) {
				
				if (multi[x][y] == 1) {
					Blocks b = new Blocks(y * 50, x * 50);
					this.actors.add(b);
					this.player1.addBlock(b);

				}
			}
		}

	}

	public void addActor(Actor actor) {
		this.actors.add(actor);
	}

	

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		for (Actor actor : this.actors) {
			actor.render(graphics);

		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {

		for (Actor actor : this.actors) {
			actor.update(gc, delta);

		}

	}

	public static void main(String[] argv) {

		try {
			AppGameContainer container = new AppGameContainer(new Game());
			container.setDisplayMode(10 *GRID_SIZE+400, 10*GRID_SIZE+300, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
