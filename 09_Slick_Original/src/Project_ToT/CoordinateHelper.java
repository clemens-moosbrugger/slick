package Project_ToT;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Point;

import Project_ToT.Blocks;
public class CoordinateHelper {
	
	public static Point getPointForBlock(Blocks block) {
		Point p = new Point((int)block.getX() * Game.GRID_SIZE - Game.GRID_SIZE ,(int) block.getY() * Game.GRID_SIZE);
		return p;

	}

	public static Point getPointForBlock(int x, int y) {
		Point p = new Point(x * Game.GRID_SIZE, y * Game.GRID_SIZE);
		return p;

	}
	
}
