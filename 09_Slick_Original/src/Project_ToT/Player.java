package Project_ToT;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Player implements Actor {
	double x;
	double y;
	private double speed;
	private Shape shape;
	private List<Shape> collisionspartner;
	private List<Blocks> blocks;
	private double velY = 0;
	private double velX = 0;
	private double gravity = 0.0001;
	private boolean falling = true;
	private boolean jumping = false;
	private final double MAX_SPEED = 10;

	public Player(double x, double y, double speed) {
		super();

		this.x = x;
		this.y = y;
		this.speed = speed;
		this.shape = new Rectangle((float) this.x, (float) this.y, 50, 50);
		this.collisionspartner = new ArrayList<>();
		this.blocks = new ArrayList<>();
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
		x += velX;
		y += velY;

		if (falling || jumping) {
			velY += gravity;
			if (velY > MAX_SPEED) {
				velY = MAX_SPEED;
			}
		}

		if (gc.getInput().isKeyDown(Input.KEY_SPACE)) {
			this.y -= delta * speed;
			this.falling = false;
			this.jumping = true;

		}
		if (gc.getInput().isKeyDown(Input.KEY_LEFT)) {
			this.x -= delta * speed;
		}
		if (gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
			this.x += delta * speed;
		}
		
		for (Blocks b : blocks) {
			if (this.y +50 == b.getY() && this.x+50 > b.getX()) {
				this.falling = false;
				System.out.println("colision");
			}
		}
		
		this.shape.setX((float) this.x);
		this.shape.setY((float) this.y);
		for (Shape shape : collisionspartner) {

			if (shape.intersects(this.shape)) {
				System.out.println("Collison with border");
				this.x -= 1;
				this.y -= 1;
			}

		}

	}

	public void addCollissionPartner(Shape s) {
		this.collisionspartner.add(s);
	}
	public void addBlock(Blocks b) {
		this.blocks.add(b);
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(Color.orange);
		graphics.fillRect((int) this.x, (int) this.y, 50, 50);
		graphics.draw(this.shape);
		graphics.setColor(Color.white);
		graphics.draw(shape);

	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

}
