package Project_ToT;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import Project_ToT.Game;

public class Blocks implements Actor {
	private int x, y;
	private Shape shape;
	private List<Shape> collisonBorder;

	public Blocks(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.shape = new Rectangle(this.x, this.y, 50, 50);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public Shape getShape() {
		return shape;
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
	}

	@Override
	public void render(Graphics graphics) {

		Point p = CoordinateHelper.getPointForBlock(this);
		graphics.setColor(Color.blue);
		graphics.fillRect(p.getX(), p.getY(), Game.GRID_SIZE, Game.GRID_SIZE);
		graphics.setColor(Color.orange);
		graphics.draw(this.shape);
	}
}
