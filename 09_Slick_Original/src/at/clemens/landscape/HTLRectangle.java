package at.clemens.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class HTLRectangle implements Actor {
	private double x, y, degree;
	private double speed;
	private int width, height;
	private Shape shape;
	

	public HTLRectangle(double x, double y, double degree, int width, int height, double speed) {
		super();
		
		this.x = x;
		this.y = y;
		this.degree = degree;
		this.width = width;
		this.height = height;
		this.speed = speed;
		this.shape =  new Rectangle((float)this.x,(float) this.y, 100,100);
	}

	public void update(GameContainer gc, int delta) {
		double xLocal = (int) (200 + 200 * Math.sin(degree += delta * speed));
		double yLocal = (int) (200 + 200 * Math.cos(degree += delta * speed));
		this.x = (int) xLocal;
		this.y = (int) yLocal;
		
		this.shape.setX((float)this.x);
		this.shape.setY((float)this.y);
	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.blue);
		graphics.fillRect((int) this.x, (int) this.y, this.width, this.height);
		graphics.setColor(Color.white);
		graphics.draw(shape);

	}

	public Shape getShape() {
		return shape;
	}
	
	

}
