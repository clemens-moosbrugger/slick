package at.clemens.landscape;


import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Player implements Actor {

	private double x, y,speed;
	private Shape shape;
	private List<Shape> collisionspartner;
	


	public Player(double x, double y, double speed) {
		super();
		this.collisionspartner = new ArrayList<>();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.shape =  new Rectangle((float)this.x,(float) this.y, 50,50);
	}

	@Override
	public void update(GameContainer gc, int delta)  {
		if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			this.y += delta*speed;
			
		}
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			this.y -= delta*speed;
		}
		if(gc.getInput().isKeyDown(Input.KEY_LEFT)) {
			this.x -= delta*speed;
		}
		if(gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
			this.x += delta*speed;
		}
		
		
		this.shape.setX((float)this.x);
		this.shape.setY((float)this.y);
		
		for (Shape shape : collisionspartner) {
			
			if( shape.intersects(this.shape)) {
				System.out.println("collison");
			
			}
		}
	}
	
	public void addCollissionPartner(Shape s) {
		this.collisionspartner.add(s);
	}

	@Override
	public void render(Graphics graphics) {
		graphics.setColor(Color.orange);
		graphics.fillRect((int) this.x, (int) this.y, 50, 50);
		graphics.setColor(Color.white);
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
