package at.clemens.landscape;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class Landscape extends BasicGame {

	private ArrayList<Actor> actors;
	
	private Player player1;
	private Picture pic;

	public Landscape() {
		super("Landscape");

	}

	@Override
	public void init(GameContainer gc) throws SlickException {

		this.actors = new ArrayList<>();	
		this.pic = new Picture("images/klaus.png", 100, 100);
		this.actors.add(pic);
		HTLRectangle rect = new HTLRectangle(100, 100, 0, 100, 100, 0.001);
		this.actors.add(rect);
		this.actors.add(new HTLOval(700, 100, 50, 100, 0.5));
		this.actors.add(new HTLCircle(100, 100, 50, 30, 0.5));

		for (int i = 0; i < 50; i++) {
			this.actors.add(new Snowflakes("s"));
		}
		for (int i = 0; i < 50; i++) {
			this.actors.add(new Snowflakes("m"));
		}
		this.player1 = new Player(100, 200, 0.5);
		this.player1.addCollissionPartner(rect.getShape());
		this.actors.add(this.player1);
		for (int i = 0; i < 50; i++) {
			this.actors.add(new Snowflakes("b"));
		}

	}

	public void addActor(Actor actor) {
		this.actors.add(actor);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		for (Actor actor : this.actors) {
			actor.render(graphics);

		}
	}

	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);
		if (key == Input.KEY_SPACE) {
			Actor bullet = new Bullet(this.player1.getX(), this.player1.getY(), 0.4);
			this.actors.add(bullet);
		}
	}

	public void keyKlausPressed(int key, char c) {
	// TODO Auto-generated method stub
		super.keyPressed(key, c);
		if (key == Input.KEY_R) {
			Actor mathbullet = new MathBullet(this.pic.getX(), this.pic.getY(), 0.4, "images/klaus.png");
			this.actors.add(mathbullet);
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {

		for (Actor actor : this.actors) {
			actor.update(gc, delta);

		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
