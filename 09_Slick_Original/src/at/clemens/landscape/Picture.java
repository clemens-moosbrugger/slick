package at.clemens.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;

public class Picture implements Actor {

	private Image image;
	private String path;
	private double width, height,x,y;
	private double speed = 0.5;
	

	public Picture(String path, double width, double height) {
		super();
		this.x = 50;
		this.y = 200;
		this.path = path;
		this.width = width;
		this.height = height;

		try {
			image = new Image(this.path);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void update(GameContainer gc, int delta) {
		if (gc.getInput().isKeyDown(Input.KEY_S)) {
			this.y += delta * speed;

		}
		if (gc.getInput().isKeyDown(Input.KEY_W)) {
			this.y -= delta * speed;
		}
		if (gc.getInput().isKeyDown(Input.KEY_A)){
			this.x -= delta * speed;
		}
		if (gc.getInput().isKeyDown(Input.KEY_D)){
			this.x += delta * speed;
		}

		
		
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public void render(Graphics graphics) {

		image.draw((float)this.x, (float) this.y, (float) this.width, (float) this.height);
	}

}
