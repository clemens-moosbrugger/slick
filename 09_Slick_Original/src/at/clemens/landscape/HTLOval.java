package at.clemens.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLOval implements Actor{

	private double x, y;
	private int width, heigth;
	private double speed;

	public HTLOval(double x, double y, int width, int heigth, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.heigth = heigth;
		this.speed = speed;
	}

	public void update(GameContainer gc, int delta) {
		if (this.y < 600) {
			this.y += delta * this.speed;
		}
		if (this.y > 550) {
			this.y = 0;
		}

	}

	public void render(Graphics graphics) {
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.heigth);
	}

}
