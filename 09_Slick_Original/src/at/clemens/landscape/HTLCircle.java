package at.clemens.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLCircle implements Actor {
	private boolean isCircleGoingRight = true;
	private int width, heigth;
	private double x, y;
	private double speed = 0.3;

	public HTLCircle(int with, int heigth, double x, double y, double speed) {
		super();

		this.width = with;
		this.heigth = heigth;
		this.x = x;
		this.y = y;
		this.speed = speed;
	}

	public void update(GameContainer gc, int delta) {
		// circle links rechts
		if (this.isCircleGoingRight) {
			this.x += delta * speed;
			if (this.x > 700) {
				this.isCircleGoingRight = false;
			}
		} else {
			this.x -= delta * speed;
			if (this.x < 100) {
				this.isCircleGoingRight = true;
			}
		}
	}

	public void render(Graphics graphics) {
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.heigth);
	}
}
