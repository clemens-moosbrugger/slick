package at.clemens.landscape;

import java.awt.Shape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowflakes implements Actor {

	private String groese;
	private double speed;
	private int size;
	private double x;
	private double y;

	

	public Snowflakes(String groese) {
		super();

		this.groese = groese;

		setpos();

		if (groese == "b") {
			size = 30;
			speed = 0.25;
		} else if (groese == "m") {
			size = 20;
			speed = 0.15;

		} else {
			size = 10;
			speed = 0.1;
		}
		

	}

	public void setpos() {
		this.x = Math.random() * 800;
		this.y = Math.random() * -600;

	}

	@Override
	public void update(GameContainer gc, int delta) {
		this.y += delta * speed;
		if (y > 600) {
			setpos();
		}

	}

	@Override
	public void render(Graphics graphics) {
		graphics.fillOval((int) this.x, (int) this.y, this.size, this.size);
	

	}

}
