package at.clemens.landscape;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class MathBullet implements Actor{
	private double x,y;
	private double speed ;
	private double centerY;
	private Image img;
	private String path;

	public MathBullet(double x, double y, double speed, String path) {
		super();
		this.path = path;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.centerY = centerY;
		try {
			img = new Image(this.path);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(GameContainer gc, int delta) {
		this.x += delta*this.speed;
		this.y = Math.pow(2, 1+ (this.x/100))+ this.centerY;

	}
	

	@Override
	public void render(Graphics graphics) {
		
		img.draw((float)this.x, (float) this.y);
	
		
	}

	

}
