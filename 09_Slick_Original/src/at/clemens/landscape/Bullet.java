package at.clemens.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Bullet implements Actor {

	private double x,y;
	private double speed ;
	private double centerY;
	
	
	public Bullet(double x, double y, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.centerY = y;
	}
	
	
	public Bullet(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		
	}

	@Override
	public void update(GameContainer gc, int delta) {
		this.x += delta*this.speed;
		this.y = Math.pow(2, 1+ (this.x/100))+ this.centerY;

	}

	@Override
	public void render(Graphics graphics) {
		
		graphics.setColor(Color.blue);
		graphics.fillOval((int)this.x, (int)this.y, 10, 10);
		graphics.setColor(Color.white);
	}



}